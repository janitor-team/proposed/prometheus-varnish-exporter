prometheus-varnish-exporter (1.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * d/p/go-issue-31859.patch: remove unneeded patch
  * d/rules: do not pass --with=systemd to dh, it is the default
  * d/watch: bump to version 4
  * Bump debhelper compatibility level to 13
  * Declare compliance with Debian Policy 4.5.1
  * d/control: rules does not require root
  * d/copyright: use secure url in Format field
  * d/control: add Pre-Depends: ${misc:Pre-Depends}

 -- Lucas Kanashiro <kanashiro@debian.org>  Thu, 28 Jan 2021 20:24:05 -0300

prometheus-varnish-exporter (1.5.2-1) unstable; urgency=high

  * New upstream version
  * Add debian/patches/go-issue-31859.patch to fix FTBFS with go >= 1.13
    (Closes: #946215)

 -- Emanuele Rocca <ema@debian.org>  Sun, 15 Mar 2020 13:21:57 +0100

prometheus-varnish-exporter (1.5-1) unstable; urgency=medium

  * New upstream version
  * Set Standards-Version to 4.4.0

 -- Emanuele Rocca <ema@debian.org>  Sun, 28 Jul 2019 19:48:19 +0200

prometheus-varnish-exporter (1.4.1-1) unstable; urgency=medium

  [ Emanuele Rocca ]
  * New upstream version 1.4.1
  * Bump debhelper compatibility level to 10, drop build dependency on
    dh-systemd
  * Pass --home to adduser in postinst to fix the Lintian error:
    maintainer-script-should-not-use-adduser-system-without-home
  * Use Priority: optional instead of extra. The latter has been deprecated
  * Set Standards-Version to 4.3.0

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org

 -- Emanuele Rocca <ema@debian.org>  Sat, 23 Feb 2019 12:07:44 +0100

prometheus-varnish-exporter (1.2-1) unstable; urgency=medium

  * New upstream version

 -- Filippo Giunchedi <filippo@debian.org>  Tue, 29 Nov 2016 00:02:59 +0000

prometheus-varnish-exporter (1.0-2) unstable; urgency=medium

  * Rename installed binary to prometheus-varnish-exporter
  * Don't ship source code from /usr/share/gocode
  * Ship SysV init script and systemd unit file
  * Create prometheus user/group on postinst

 -- Filippo Giunchedi <filippo@debian.org>  Wed, 14 Sep 2016 13:44:13 +0100

prometheus-varnish-exporter (1.0-1) unstable; urgency=medium

  * Initial release (Closes: #837089)

 -- Emanuele Rocca <ema@debian.org>  Fri, 09 Sep 2016 10:53:53 +0200
